package com.example.temgallery;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;

public class ImageDatabase {
    ImageDBOpenHelper helper;
    private static final int BLOB_IMAGE_COLUMN = 1;
    private static final int TITLE_COLUMN = 0;


    public ImageDatabase(Context context){
        helper = new ImageDBOpenHelper(context);
    }

    public long addImage(String imageName, Bitmap image){
        //add image if it is in bitmap format
        byte[] imageArray = this.getBitmapAsByteArray(image);
        return addImage(imageName, imageArray);
    }

    public long addImage(String imageName, byte[] image){
        //add image if it is in bytearray format
        ContentValues values = new ContentValues();
        values.put("image_name", imageName);
        values.put("image_blob", image);

        //get database then add contentvalue to image_table
        SQLiteDatabase database = helper.getWritableDatabase();
        long result = database.insert("image_table", null, values);
        return result;
    }

    public int getImageCount(){
        SQLiteDatabase readDatabase = helper.getReadableDatabase();
        Cursor testCursor = readDatabase.query("image_table", null, null, null, null, null, null);
        return testCursor.getCount();

    }

    public static byte[] getBitmapAsByteArray(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream);
        return outputStream.toByteArray();
    }

    public String getImageNameByIndex(int i){
        SQLiteDatabase readDatabase = helper.getReadableDatabase();
        Cursor testCursor = readDatabase.query("image_table", null, null, null, null, null, null);
        if(i < testCursor.getCount() && i >= 0){
            return testCursor.getString(TITLE_COLUMN);
        }
        if (testCursor != null && !testCursor.isClosed()) {
            testCursor.close();
        }
        return null;
    }

    public String getImageNameByRow(int row){
        int index = row-1;
        return getImageNameByIndex(index);
    }

    public Bitmap getImageByIndex(int i){
        SQLiteDatabase readDatabase = helper.getReadableDatabase();
        Cursor cursor = readDatabase.query("image_table", null, null, null, null, null, null);
        if(i < cursor.getCount() && i >= 0){
            cursor.moveToPosition(i);
            byte[] imgByte = cursor.getBlob(BLOB_IMAGE_COLUMN);
            return BitmapFactory.decodeByteArray(imgByte, 0, imgByte.length);
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return null;
    }

    public Bitmap getImageByRow(int row){
        int index = row-1;
        return getImageByIndex(index);
    }

}
